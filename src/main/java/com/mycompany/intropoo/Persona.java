
package com.mycompany.intropoo;

/*
 * Nombre
 * Apellido
 * Edad
 * Color cabello
 * Color de ojos
 * Estatura
 * Peso
 * Género
 */

public class Persona {
    //ATRIBUTOS
    public String nombre;
    public String apellido;
    public int edad;
    public char genero;

    //Método constructor
    public Persona(String nombre, String apellido, int edad, char genero){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.genero = genero;
        //System.out.println("Soy un objeto persona");
    }

    //Métodos consultores
    public String getNombre(){
        return this.nombre;
    }
    public String getApellido(){
        return this.apellido;
    }
    public int getEdad(){
        return this.edad;
    }
    public char getGenero(){
        return this.genero;
    }

    //Métodos modificadores
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    public void setGenero(char genero){
        this.genero = genero;
    }

    public double calcularNomina(int cantHoras){
        double valorHora = 0;
        double nomina = 0.0;

        if(edad >= 18 && edad <= 22){
            valorHora = 10;
        }else if(edad >= 23 && edad <= 25){
            valorHora = 15.2;
        }else if(edad >= 26 && edad <= 30){
            valorHora = 18.5;
        }else{
            valorHora = 20.2;
        }

        nomina = cantHoras * valorHora;

        return nomina;
    }

}
