
package com.mycompany.intropoo;


public class IntroPOO {

    public static void main(String[] args) {
        //Crear objeto de tipo Persona
        Persona objPersona_1 = new Persona("Paula", "Fernandez", 20, 'F');
        //Obtener el nombre de la persona 1
        String nombre = objPersona_1.getNombre();
        System.out.println("El nombre de objPersona_1 es : "+nombre);
        System.out.println("El apellido de objPersona_1 es:"+objPersona_1.getApellido());
        //Modificar nombre
        objPersona_1.setNombre("María");
        System.out.println("El nombre de objPersona_1 es : "+objPersona_1.getNombre());
        /*************************************
         * Crear varias personas
         */
        Persona objPersona_2 = new Persona("Jose", "Torres", 22, 'M');
        objPersona_2.setApellido("Montero");
        Persona objPersona_3 = new Persona("Angel", "Montero", 26, 'M');
        Persona objPersona_4 = new Persona("Angélica", "Mendez", 32, 'F');

        /***********************
         * NOMINAS
         *****************/
        double nomina_1 = objPersona_1.calcularNomina(10);
        double nomina_2 = objPersona_2.calcularNomina(10);
        double nomina_3 = objPersona_3.calcularNomina(10);
        double nomina_4 = objPersona_4.calcularNomina(10);

        System.out.println("ObjPersona_1-> "+nomina_1);
        System.out.println("ObjPersona_2-> "+nomina_2);
        System.out.println("ObjPersona_3-> "+nomina_3);
        System.out.println("ObjPersona_4-> "+nomina_4);


    }
}
